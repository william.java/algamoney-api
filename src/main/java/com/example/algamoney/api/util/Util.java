package com.example.algamoney.api.util;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.example.algamoney.api.exceptionhandler.AlgamoneyExceptionHandler.Erro;

public class Util {
	public static List<Erro> retornaListaDeErrosTratados(RuntimeException ex, String nomeDoRecurso, boolean comCausa, MessageSource messagesource) {
		String mensagemUsuario = messagesource.getMessage(nomeDoRecurso, null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = "";
		if (comCausa)
			mensagemDesenvolvedor = ExceptionUtils.getRootCauseMessage(ex);
		else
			mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return erros;
	}

}
