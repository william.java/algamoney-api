CREATE TABLE public.lancamento
(
    codigo serial NOT NULL,
    descricao character varying(50) NOT NULL,
    data_vencimento date NOT NULL,
    data_pagamento date,
    valor numeric(10, 2) NOT NULL,
    observacao character varying(100),
    tipo character varying(20) NOT NULL,
    codigo_categoria integer NOT NULL,
    codigo_pessoa integer NOT NULL,
    CONSTRAINT pk_lancamento PRIMARY KEY (codigo),
    CONSTRAINT fk_catergoria FOREIGN KEY (codigo_categoria)
        REFERENCES public.categoria (codigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pessoa FOREIGN KEY (codigo_pessoa)
        REFERENCES public.pessoa (codigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.lancamento
    OWNER to postgres;

insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Sal�rio Mensal', '2017-06-10', null, 6500.00, 'Distribui��o de lucros', 'RECEITA', 1, 1);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Bahamas', '2017-02-10', '2017-02-10', 100.32, null, 'DESPESA', 2, 2);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Top Club', '2017-06-10', null, 120, null, 'RECEITA', 3, 3);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('CEMIG', '2017-10-02', '2017-02-10', 110.44, 'Gera��o', 'RECEITA', 3, 4);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('DMAE', '2017-06-10', null, 200.30, null, 'DESPESA', 3, 5);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Extra', '2017-03-10', '2017-03-10', 1010.32, null, 'RECEITA', 4, 6);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Extra', '2017-03-10', '2017-03-10', 1010.32, null, 'RECEITA', 4, 6);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Bahamas', '2017-06-10', null, 500, null, 'RECEITA', 1, 5);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Top Club', '2017-03-10', '2017-03-10', 400.32, null, 'DESPESA', 4, 8);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Despachante', '2017-06-10', null, 123.64, 'Multas', 'DESPESA', 3, 9);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Pneus', '2017-04-10', '2017-04-10', 665.33, null, 'RECEITA', 5, 10);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Caf�', '2017-06-10', null, 8.32, null, 'DESPESA', 1, 5);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Eletr�nicos', '2017-04-10', '2017-04-10', 2100.32, null, 'DESPESA', 5, 4);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Instrumentos', '2017-06-10', null, 1040.32, null, 'DESPESA', 4, 3);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Caf�', '2017-04-10', '2017-04-10', 4.32, null, 'DESPESA', 4, 2);
insert into lancamento (descricao, data_vencimento, data_pagamento, valor, observacao, tipo, codigo_categoria, codigo_pessoa)
values ('Lanche', '2017-06-10', null, 10.20, null, 'DESPESA', 4, 1);