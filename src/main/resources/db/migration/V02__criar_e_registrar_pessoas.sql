CREATE TABLE pessoa
(
    codigo serial NOT NULL,
    nome character varying(100) NOT NULL,
    ativo boolean NOT NULL,
    logradouro character varying(50),
    numero character varying(10),
    bairro character varying(50),
    cep character varying(20),
    cidade character varying(50),
    estado character varying(20),
    CONSTRAINT pk_pessoa PRIMARY KEY (codigo)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE pessoa OWNER to postgres;

insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Wilker', true, 'Rua Professor Jos�', '1010', 'Guajeru', '60860000', 'Fortaleza', 'CE');
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Anderson', true, 'Rua Professor Jos�', '1010', 'Guajeru', '60860000', 'Fortaleza', 'CE');
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Hugo', true, 'Rua Marechal', '2210', 'Guajeru', '60860000', 'Fortaleza', 'CE');
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('P�mela', true, 'Rua mmaa', '1010', 'Piraj�', '60860030', 'Juazeiro do Norte', 'CE');
insert into pessoa (nome, ativo) values ('Karla Porto', true);
insert into pessoa (nome, ativo) values ('Gabriel', true);
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Uribe', true, 'Rua Limoeiro', '1040', 'cai�u', '60860000', 'Lolok', 'PI');				
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Marta', true, 'Rua Lumia Jos�', '10', 'Guajeru', '60860000', 'Fortaleza', 'CE');
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Anderson Silva', true, 'Rua Dr. Miguel', '4410', 'Guaruj�', '10860000', 'S�o Paulo', 'SP');
insert into pessoa (nome, ativo, logradouro, numero, bairro, cep, cidade, estado) 
				    values ('Thais Rebol�as', true, 'Rua Jos� Am�lio', '1000', 'Guajeru', '60860000', 'Fortaleza', 'CE');
insert into pessoa (nome, ativo) values ('Gerson', false);