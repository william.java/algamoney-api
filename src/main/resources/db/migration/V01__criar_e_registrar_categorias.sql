CREATE TABLE categoria
(
    codigo serial NOT NULL,
    nome character varying(50) NOT NULL,
    CONSTRAINT pk_categoria PRIMARY KEY (codigo)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE categoria OWNER to postgres;

insert into categoria (nome) values ('Lazer');
insert into categoria (nome) values ('Alimenta��o');
insert into categoria (nome) values ('Supermercado');
insert into categoria (nome) values ('Farm�cia');
insert into categoria (nome) values ('Outros');